<?php
$msg=isset($msg)?($msg):"";
require_once 'DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
$manufacturer = $dao->selectManufacturer();
$brands = $dao->selectBrands();
$brandid=$dao->selectBrandWithId();
$typeid=$dao->selectProductType();
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../shop-page/shop.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container cards">
        <div class="row">
            <div class="col-3 home">
                <p>Home > <b>Contact us</b></p>
                <section class="left-box">
                    <h2>Categories</h2>
                    <input type="checkbox" name="mobile" id="mobile" value=""> Mobiles
                    <br>
                    <input type="checkbox" name="laptop" id="laptop"> Laptops
                    <br>
                    <input type="checkbox" name="television" id="tv"> Television
                    <br>
                </section>
                <section class="left-box">
                    <h2>Price range</h2>
                    <form id="price-range-form">
                        <label for="min-price" class="form-label">Min price: </label>
                        <span id="min-price-txt">0 din</span>
                        <input type="range" class="form-range" min="0" max="150000" id="min-price" step="1" value="0"><br>
                        <label for="max-price" class="form-label">Max price: </label>
                        <span id="max-price-txt">150000 din</span>
                        <input type="range" class="form-range" min="1" max="150000" id="max-price" step="1" value="150000">
                    </form>
                </section>
                <section class="left-box">
                    <h2>Brand</h2>
                    <?php foreach ($brands as $pom => $value) {
                        echo '<label for="value' . $pom . '">' . $value['brand_name']  . '</label>';
                        echo '<input type="checkbox" name="' . $value['brand_name'] . '" id="' . $value['brand_name'] . '" value="' . $value['brand_name'] . '" /><br>';
                    } ?>
                </section>
                <section class="left-box">
                    <h2>Manufacturer</h2>
                    <?php foreach ($manufacturer as $pom => $value) {
                        echo '<label for="value' . $pom . '">' . $value['manufacturer_name'] . '</label>';
                        echo '<input type="checkbox" name="' . $value['manufacturer_name'] . '" id="' . $value['manufacturer_name'] . '" value="' . $value['manufacturer_name'] . '" /><br>';
                    } ?>
                </section>
                <section class="left-box">
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                </section>
            </div>
            <div class="col-9 producta" id="producta">
                    <table class="table">
                        <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Image</th>
                        <th>Delete</th>
                        </tr>
                        <?php foreach ($products as $pom){ ?>
                        <tr>
                                <td><?=$pom['id']  ?></td>
                                <td><?=$pom['name']  ?></td>
                                <td><?=$pom['model']  ?></td>
                                <td><?=$pom['color']  ?></td>
                                <td><img src="../images/<?=$pom['image']  ?>" style="width: 100px;height:100px" alt=""></td>
                                <td><a href="../login-page/login-controller.php?action=delete&id=<?=$pom["id"]?>">Delete</a></td>
                        </tr>
                        <?php } ?>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                        <form action="admin-controller.php" method="POST">
            <h1>Insert product</h1>
            Id type: <br> <input type="text" name="type" placeholder="Insert type id"><br>
            Price: <br> <input type="text" name="price" placeholder="Insert price"><br>
            Name: <br> <input type="text" name="name" placeholder="Insert product name"><br>
            Model: <br> <input type="text" name="model" placeholder="Insert product model"><br>
            Color: <br> <input type="text" name="color" placeholder="Insert product color"><br>
            Id brand: <br> <input type="text" name="brand" placeholder="Insert brand id"><br>
            Image: <br> <input type="file" name="image"><br>
            Insert other images with link: <br>
            <input type="text" name="image_2" placeholder="Insert image link"><br>
            <input type="text" name="image_3" placeholder="Insert image link"><br>
            <input type="text" name="image_4" placeholder="Insert image link"><br>
            <input type="submit" name="action" value="Insert">
            </form>
            <?=$msg  ?>

            </div>  
            <div class="col-4">
                <h1>Types with id</h1>
                <?php foreach ($brandid as $key){ ?>
                <ul>
                <li>Id: <?=$key['brand_id']  ?> for type <?=$key['brand_name'] ?></li>
                </ul>
                <?php } ?>
            </div>
            <div class="col-4">
                <h1>Brands with id</h1>
                <?php foreach ($typeid as $key){ ?>
                <ul>
                <li><?=$key['id_product_type']  ?>-<?=$key['type'] ?></li>
                </ul>
                <?php } ?>
            </div>
    </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
    

    <script>
        var products =
            <?php echo json_encode($products);

            ?>;
    </script>
