<?php
require_once 'DAO-shop.php';
$dao = new DAOSHOP();
$output = '';
if(isset($_POST["query"]))
{
	$search = $_POST["query"];
    $search = $str = strtoupper($search."%");
    $result = $dao->queryProducts($search);
}
else
{
	$result = $dao->selectProducts();
}
if($result != null)
{

	foreach($result as $row)
	{
		$output .= '`<div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);width: 15rem;text-align: center;font-family: arial;display: inline-block;margin-left:2rem;margin-bottom:2rem">
		<img src="../images/'.$row["image"].'" style="width:200px;height:200px;">
		<h2>'.$row["name"].'</h2>
		<h3>'.$row["model"].'</h3>
		<p class="price" style="color: grey;font-size: 22px;"><b>'.$row["price"].' din</b></p>
		<a href="../products-page/product-controller.php?action=product&id='.$row['id'].'"><p><button style="border: none;outline: 0;padding: 12px;color: white;background-color: #78ac57;text-align: center;cursor: pointer;width: 100%;font-size: 18px;opacity:0.7">View product</button></p></a>
	  </div>`';
	}
	echo $output;
}
else
{
	echo 'Data Not Found';
}
?>
