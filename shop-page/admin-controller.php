<?php
require_once '../shop-page/DAO-shop.php';
session_start();
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";


if ($_SERVER['REQUEST_METHOD'] = "POST") {
    if ($action == 'Insert') {
        $type = isset($_POST["type"]) ? test_input($_POST["type"]) : "";
        $price = isset($_POST["price"]) ? test_input($_POST["price"]) : "";
        $name = isset($_POST["name"]) ? test_input($_POST["name"]) : "";
        $model = isset($_POST["model"]) ? test_input($_POST["model"]) : "";
        $color = isset($_POST["color"]) ? test_input($_POST["color"]) : "";
        $brand = isset($_POST["brand"]) ? test_input($_POST["brand"]) : "";
        $image = isset($_POST["image"]) ? test_input($_POST["image"]) : "";
        $image_2 = isset($_POST["image_2"]) ? test_input($_POST["image_2"]) : "";
        $image_3 = isset($_POST["image_3"]) ? test_input($_POST["image_3"]) : "";
        $image_4 = isset($_POST["image_4"]) ? test_input($_POST["image_4"]) : "";

        if ($type == '' || $price == '' || $name == '' || $color == '' || $brand == '' || $image == '') {
            $msg = '<br><span style="color: red;">Please fill in all fields!!';
            include_once '../shop-page/admin.php';
        } elseif (!is_numeric($price) || !is_numeric($type) || !is_numeric($brand)) {
            $msg = '<br><span style="color: red;">Wrong insert!!!';
            include_once '../shop-page/admin.php';
        } else {
            $dao = new DAOSHOP();
            $dao->insertProduct($type, $price, $name, $model, $color, $brand, $image, $image_2, $image_3, $image_4);
            include '../shop-page/admin.php';
        }
    } elseif ($action == 'a') {
    }
}
if ($_SERVER['REQUEST_METHOD'] = "GET") {

    if ($action === 'logout') {
        session_start();
        session_unset();
        session_destroy();

        header('Location: product.php');
    } elseif ($action == 'akcijaGet2') {
    } elseif ($action == 'akcija3') {
    }
} else {
    die();
}
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
