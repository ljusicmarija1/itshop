<?php
require_once '../database/db.php';

class DAOSHOP {
	private $db;
	private $SELECTPRODUCTS = "SELECT * FROM products JOIN brands ON products.id_brand=brands.brand_id JOIN manufacturers ON brands.id_manufacturer=manufacturers.id_manufacturer JOIN product_types on products.id_type=product_types.id_product_type ORDER BY id ASC";
	private $SELECTPRODUCTSBYID = "SELECT * FROM products JOIN brands ON products.id_brand=brands.brand_id JOIN manufacturers ON brands.id_manufacturer=manufacturers.id_manufacturer JOIN product_types on products.id_type=product_types.id_product_type WHERE id=?";
	private $SELECT_BRAND_NAME = "SELECT brand_name FROM brands";
	private $SELECT_MANUFACTURER_NAME = "SELECT manufacturer_name FROM manufacturers";
	private $DELETE_PRODUCT = "DELETE FROM products WHERE id=?";
	private $QUERY_FOR_SEARCH = "SELECT * FROM products WHERE name LIKE UPPER(:NAME)";
	private $INSERT_PRODUCT= "INSERT INTO products (`id`, `id_type`, `price`, `name`, `model`, `color`, `id_brand`, `image`,`image_2`,`image_3`,`image_4`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?,?,?,?)";
	private $SELECT_BRANDS_ID = "SELECT brand_id,brand_name FROM brands ORDER BY brand_id ASC";
	private $SELECT_TYPE_ID = "SELECT id_product_type,type FROM product_types ORDER BY id_product_type ASC";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}
	public function selectProducts()
	{
		
		$statement = $this->db->prepare($this->SELECTPRODUCTS);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectBrandWithId()
	{
		
		$statement = $this->db->prepare($this->SELECT_BRANDS_ID);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectProductType()
	{
		
		$statement = $this->db->prepare($this->SELECT_TYPE_ID);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectProductsById($id)
	{
		
		$statement = $this->db->prepare($this->SELECTPRODUCTSBYID);
		$statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectBrands()
	{
		
		$statement = $this->db->prepare($this->SELECT_BRAND_NAME);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
		public function selectManufacturer()
	{
		$statement = $this->db->prepare($this->SELECT_MANUFACTURER_NAME);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function queryProducts($query)
	{
		$statement = $this->db->prepare($this->QUERY_FOR_SEARCH);
		$statement->bindValue(":NAME",$query);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function deleteProduct($id)
	{
		$statement = $this->db->prepare($this->DELETE_PRODUCT);
		$statement->bindValue(1, $id);
		
		$statement->execute();
	}

	public function insertProduct($type, $price,$name,$model,$color,$brand,$image,$image_2,$image_3,$image_4)
	{

		$statement = $this->db->prepare($this->INSERT_PRODUCT);
		$statement->bindValue(1, $type);
		$statement->bindValue(2, $price);
		$statement->bindValue(3, $name);
		$statement->bindValue(4, $model);
		$statement->bindValue(5, $color);
		$statement->bindValue(6, $brand);
		$statement->bindValue(7, $image);
		$statement->bindValue(8, $image_2);
		$statement->bindValue(9, $image_3);
		$statement->bindValue(10, $image_4);
		
		$statement->execute();
	}
}
?>