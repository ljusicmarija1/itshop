<?php
require_once 'DAO-login.php';
require_once '../shop-page/DAO-shop.php';
session_start();
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";


if ($_SERVER['REQUEST_METHOD'] = "POST") {
    if ($action == 'Login') {
        $username = isset($_POST["username"]) ? test_input($_POST["username"]) : "";
        $password = isset($_POST["password"]) ? test_input($_POST["password"]) : "";

        if ($username == "" || $password == "") {
            $msg = "<p style='color: red;margin-left:24.5rem'>Fill in all fields!!</p>";
            include_once '../login-page/login.php';
        } else {

            $dao = new DAO;
            $user = $dao->selectUserByUsernameAndPassword($username, $password);
            if ($user) {
                $_SESSION['user'] = $user;
                $_SESSION['last-active'] = time();
                if (isset($_POST['remember'])) {
                    $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);

                    setcookie("userJSON", json_encode($userCookie), time() + 20, "/");
                }
               if($user["type"]=='admin'){
                   include '../shop-page/admin.php';
               }else{
                   include '../shop-page/shop.php';
               }
            } else {
                $msg = "<p style='color: red;margin-left:24.5rem'>Wrong login parameters!!!</p>";
                include_once 'login.php';
            }
        }

    }
}
if ($_SERVER['REQUEST_METHOD'] = "GET") {

    if ($action === 'logout') {
            session_unset();
            session_destroy();
            include '../index/index.php';
    } elseif ($action == 'delete') {
        $dao=new DAOSHOP();
        $id=isset($_GET['id'])?$_GET['id'] : '';
        $dao->deleteProduct($_GET['id']);
        include_once '../shop-page/admin.php';
    }
} else {
    //...
    header("Location: index.php"); //opciono
    die();
}
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
