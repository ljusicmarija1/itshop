<?php
require_once 'DAO-register.php';
session_start();
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";


if ($_SERVER['REQUEST_METHOD'] = "POST") {
    if ($action == 'LOGIN') {

    } elseif ($action == 'Register') {
        $first_name = isset($_POST["name"]) ? test_input($_POST["name"]) : "";
        $last_name = isset($_POST["last_name"]) ? test_input($_POST["last_name"]) : "";
        $email = isset($_POST["email"]) ? test_input($_POST["email"]) : "";
        $phone_number = isset($_POST["number"]) ? test_input($_POST["number"]) : "";
        $username = isset($_POST["username"]) ? test_input($_POST["username"]) : "";
        $password = isset($_POST["password"]) ? test_input($_POST["password"]) : "";
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $repeat_password = isset($_POST["repeat_password"]) ? test_input($_POST["repeat_password"]) : "";

        if ($first_name == '' || $last_name == '' || $email == '' || $username == '' || $password == '' || $repeat_password == ''  || $phone_number == '') {
            $msg = '<br><span style="color: red;">Please fill in all fields!!';
            include_once 'register.php';
        }  elseif (!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            $msg = '<br><span style="color: red;">Password is not valid';
            include_once 'register.php';
        } elseif ($password != $repeat_password) {
            $msg = '<br><span style="color: red;">Passwords not matching';
            include_once 'register.php';
        } else {
            $dao = new DAO;
            if (!$dao->selectUserByUsername($username)) {
                if ($dao->insertUserWithAtributes($first_name, $last_name, $email, $username, $password,$phone_number) == false) {
                    $msg = '<br><span style="color: red;">Error!';
                    include_once 'register.php';
                } else {
                    include_once '../login-page/login.php';
                }
            } else {
                $msg = "<br><span style='color: red;'>Username '$username' is already in use!";
                include_once 'register.php';
            }
        }
    }
}
if ($_SERVER['REQUEST_METHOD'] = "GET") {

    if ($action === 'logout') {
        session_start();
        session_unset();
        session_destroy();

        header('Location: buyer.php');
    } elseif ($action == 'akcijaGet2') {
        //...
    } elseif ($action == 'akcijaGet3') {
        //...
    }
} else {
    //...
    header("Location: index.php"); //opciono
    die();
}
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
