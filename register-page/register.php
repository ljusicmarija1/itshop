<?php $msg=isset($msg)?($msg):""; ?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="register.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <form action="register-controller.php" method="POST">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    <label for="name"><b>First Name</b></label><br>
    <input type="text" placeholder="Enter Name" name="name" id="name" required><br>
    <label for="last-name"><b>Last Name</b></label><br>
    <input type="text" placeholder="Last Name" name="last_name" id="last-name" required><br>
    <label for="phone-number"><b>Phone Number</b></label><br>
    <input type="text" placeholder="Phone number" name="number" id="number" required><br>
    <label for="email"><b>Email</b></label><br>
    <input type="text" placeholder="Enter Email" name="email" id="email" required><br>
    <label for="username"><b>Username</b></label><br>
    <input type="text" placeholder="Enter Username" name="username" id="username" required><br>
    <label for="psw"><b>Password</b></label><br>
    <input type="password" placeholder="Enter Password" name="password" id="psw" required><br>
    <label for="psw-repeat"><b>Repeat Password</b></label><br>
    <input type="password" placeholder="Repeat Password" name="repeat_password" id="psw-repeat" required><br>
    <hr>
    <button type="submit" name="action" value="Register" class="registerbtn">Register</button><br>
  </div>

  <div class="container signin">
    <p>Already have an account? <a href="../login-page/login.php">Sign in</a>.</p>
  </div>
</form>
<?=$msg  ?>
        </div>
        </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
