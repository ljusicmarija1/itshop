<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="blog.css">
<title>Blog</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-3 home">
                <p>Home > <b>Contact us</b></p>
                <section class="left-box">
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                </section>
                <section class="left-box">
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                </section>
                <section class="left-box">
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                </section>
                <section class="left-box">
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                    <p class="card-text"><b>Laptops</b></p>
                </section>
            </div>
            <div class="col-9">
                <h2 class="h2-blog"><b>Lastest Blogs</b></h2>
                <img src="https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" alt="" class="img-blog">
                Posted by: <i class="blogs-text"> CNN Business </i>
                <br>
                <p><b>Google is making its Biggest Investament Ever in Australia</b></p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt quo nisi sequi a voluptatem nulla, delectus laborum hic officiis aut alias quaerat consequatur? Culpa minus assumenda, eaque omnis totam illum!
                <p class="read-more"><b> Read More</b></p>
                <hr>

                Posted by: <i class="blogs-text"> CNN Business </i>
                <br>
                <p><b>Google is making its Biggest Investament Ever in Australia</b></p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt quo nisi sequi a voluptatem nulla, delectus laborum hic officiis aut alias quaerat consequatur? Culpa minus assumenda, eaque omnis totam illum!
                <p class="read-more"><b> Read More</b></p>
                <hr>
            </div>
        </div>
    </div>
    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>