<div class="container">
        <div class="row">
            <div class="col-3">
                <img src="../images/logoshop.png" alt="" class="logo">
            </div>
            <div class="col-2">
                <section class="left-text">
                    <h6><b>Send us a message</b></h6><br>
                    <p class="mail">demo@demo.com</p>
                </section>
            </div>
            <div class="col-4">
                <section class="right-text">
                    <h6>Need Help? Call us:<br>
                        <b class="number">012 345 6789</b>
                    </h6>
                </section>
            </div>
            <div class="col-3">
                <section class="icons">
                   <a href="../login-page/login.php"></a> <i class="fa fa-user"></i>
                    <i class="fa fa-heart"></i>
                   <a href="../shop-page/cart.php"> <i class="fa fa-shopping-bag"></i></a>
                </section>
            </div>
        </div>
    </div>
    <hr>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <section class="center-section">
                        <a href="#content"><button type="button" class="btn btn-dark">TOP PRODUCTS<i
                                class="fa fa-grip-lines"></i></button></a>
                        <a href="../index/index.php">HOME <i class="fa fa-chevron-down"></i></a>
                        <?php
                        if(isset($_SESSION['user'])){
                            if($_SESSION['user']['type']=='admin'){
                            echo  '<a href="../shop-page/admin.php">SHOP <i class="fa fa-chevron-down"></i></a>';
                            }else{ echo  '<a href="../shop-page/shop.php">SHOP <i class="fa fa-chevron-down"></i></a>';}
                        }else{
                         echo  '<a href="../shop-page/shop.php">SHOP <i class="fa fa-chevron-down"></i></a>';
                        }
                        ?>
                        <a href="../blog-page/blog.php">BLOGS</a>
                        <a href="../contact-page/contact.php">CONTACT</a>
                        <a href="../orders-page/orders.php">ORDERS</a>
                    </section>
                </div>
                <div class="col-3">
                <div class="form-group">
			<div class="input-group">
				<span class="input-group-addon"></span>
				<input type="text" name="search_text" id="search_text" placeholder="Search products" class="form-control" />
			</div>
		</div>
                </div>
            </div>
        </div>
    </nav>
    <script>
	$(document).ready(function() {

		function load_data(query) {
			$.ajax({
				url: "fetchData.php",
				method: "post",
				data: {
					query: query
				},
				success: function(data) {
					$('#producta').html(data);
				}
			});
		}

		$('#search_text').keyup(function() {
			var search = $(this).val();
			if (search != '') {
				load_data(search);
			} else {
				load_data();
			}
		});
	});
</script>