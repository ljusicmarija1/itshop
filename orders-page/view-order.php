<?php
require_once 'DAO-orders.php';

if(!isset($_SESSION)) session_start();
if($_SESSION['order']!=""){
$dao = new DAO();
$ord=$dao->selectOrdersById($_SESSION['order']);

?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="orders.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
               <table class="table">
                <tr>
                    <th>ID Order</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Model</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone number</th>
                    <th>Address</th>
                    <th>City</th>
                </tr>
                <?php foreach ($ord as $pom){ ?>
                <tr>
                    <td><?=$pom['order_id']  ?></td>
                    <td><?=$pom['type']  ?></td>
                    <td><?=$pom['name']  ?></td>
                    <td><?=$pom['model']  ?></td>
                    <td><?=$pom['first_name']  ?></td>
                    <td><?=$pom['last_name']  ?></td>
                    <td><?=$pom['phone_number']  ?></td>
                    <td><?=$pom['address']  ?></td>
                    <td><?=$pom['city']  ?></td>
                    
                </tr>
                <?php } ?>
               </table>
               

        </div>
        </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
<?php } ?>