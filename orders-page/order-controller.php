<?php
    require_once 'DAO-orders.php';
session_start();

$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; //provera da li je setovana akcija


if ($_SERVER['REQUEST_METHOD']=="POST"){
    if ($action == 'Send') {
       $type = isset($_POST["type"])? test_input($_POST["type"]) : "";
       $model = isset($_POST["model"])? test_input($_POST["model"]) : "";
       $address = isset($_POST["address"])? test_input($_POST["address"]) : "";
       $city = isset($_POST["city"])? test_input($_POST["city"]) : "";
       $dao=new DAO();
       $dao->insertOrder($type,$model,$address,$city);
       include 'orders.php';


    } elseif ($action == 'update') {
        $order_id = isset($_POST["order_id"])? test_input($_POST["order_id"]) : "";
        $model = isset($_POST["model"])? test_input($_POST["model"]) : "";
        $address = isset($_POST["address"])? test_input($_POST["address"]) : "";
        $city = isset($_POST["city"])? test_input($_POST["city"]) : "";
        $dao=new DAO();
        $dao->updateOrder($model,$address,$city,$order_id);
        include_once 'orders.php';
    } 
    
    
} if ($_SERVER['REQUEST_METHOD']=="GET"){
    if ($action == 'view') {
    $order_id = isset($_GET["order_id"])? test_input($_GET["order_id"]) : "";
    $dao=new DAO();
    $_SESSION['order']=$order_id;
    include_once 'view-order.php';
    }
    
    elseif ($action == 'delete') {
        $order_id = isset($_GET["order_id"])? test_input($_GET["order_id"]) : "";
        $dao=new DAO();
        $dao->deleteOrder($_GET['order_id']);
        include 'orders.php';

    } 
    elseif ($action == 'updateor') {
        $order_id = isset($_GET["order_id"])? test_input($_GET["order_id"]) : "";
        $dao=new DAO();
        include_once 'update-order.php';
    } 
    

} else {
    //...
    header("Location: index.php"); //opciono
    die();
}

//funkcija za preradu unetih podataka
function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>