<?php
require_once 'DAO-orders.php';
$dao = new DAO();
$orders=$dao->selectProductTypes();
$msg=isset($msg)?($msg):"";
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="orders.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container">
        <div class="row">
           <form action="order-controller.php" method="POST">
               Product type: <br><select name="type">
               <?php foreach ($orders as $pom) { ?>
                            <option value="<?= $pom['type'] ?>"><?= $pom['type'] ?></option>
                        <?php } ?>
               </select><br>
               Model: <br><input type="text" name="model"><br>
               Address: <br><input type="text" name="address"><br>
               City: <br><input type="text" name="city"><br><br>
               <input type="submit" name="action" value="Send">
           </form>
        </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
