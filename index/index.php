<?php
require_once '../shop-page/DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../index/index.css">
<title>Index</title>
</head>

<body>
  <?php include_once '../partials/nav.php' ?>
  <?php include_once '../partials/header.php' ?>

  <div class="container">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="https://images.macrumors.com/t/3Z29QsveU3-DLOv_Jq1BjTKd730=/1600x1200/smart/article-new/2021/09/Apple-iPhone-13-colors-lineup-2022.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="https://images.macrumors.com/article-new/2021/09/iphone-13-pro-max-cameras.jpg" alt="Second slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <div class="container">
    <section class="text">
      <p class="text text2" style="margin-left:2rem ;"><b><a href="../shop-page/shop.php"> View all</b> </a></p>
    </section>
  </div>
  <div class="container">
    <div id="content">
      <?php foreach ($products as $pom) {

        echo  '<div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);width: 15rem;text-align: center;font-family: arial;display: inline-block;margin-left:2rem;margin-bottom:2rem">
		<img src="../images/' . $pom["image"] . '" style="width:200px;height:200px;">
		<h2>' . $pom["name"] . '</h2>
		<h3>' . $pom["model"] . '</h3>
		<p class="price" style="color: grey;font-size: 22px;"><b>' . $pom["price"] . ' din</b></p>
    <a href="../products-page/product-controller.php?action=product&id=' . $pom['id'] . '"><p><button style="border: none;outline: 0;padding: 12px;color: white;background-color: #78ac57;text-align: center;cursor: pointer;width: 100%;font-size: 18px;opacity:0.7">View product</button></p></a>
       
	  </div>';
      } ?>
    </div>
  </div>
  <?php include_once '../partials/bottom.php' ?>
  <?php include_once '../partials/footer.php' ?>