const form= {
    email: document.getElementById('email'),
    textarea: document.getElementById('textarea'),
    submit: document.getElementById('submit'),
    messages: document.getElementById('form-messages'),
    
 };
 form.submit.addEventListener('click',() =>{
 const request = new XMLHttpRequest();
 request.onload = () => {
 let responseObject=null;
 try {
     responseObject = JSON.parse(request.responseText);
     
 } catch (e) {
     console.error('Could not parse JSON!');
 }
 if (responseObject) {
     handleResponse(responseObject);
 }
 };
 const requestData= `email=${form.email.value}&textarea=${form.textarea.value}`;
 request.open('post','check-mail.php');
 request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
 request.send(requestData);
 });
 function handleResponse (responseObject) {
             if (responseObject.ok) {
                 location.href = 'contact.php';
             } else {
                 while (form.messages.firstChild) {
                     form.messages.removeChild(form.messages.firstChild);
                 }
 
                 responseObject.messages.forEach((message) => {
                     const li = document.createElement('li');
                     li.textContent = message;
                     form.messages.appendChild(li);
                 });
 
                 form.messages.style.display = "block";
             }
         }

 