<?php 
$email = isset($_POST['email']) ? $_POST['email'] : '';
$textarea = isset($_POST['textarea']) ? $_POST['textarea'] : '';

$ok = true;
$messages = array();

if ( !isset($email) || empty($email) ) {
    $ok = false;
    $messages[] = 'Fill in the email!';
}
if (!str_contains($email,'@')) {
    $ok = false;
    $messages[] = 'Email is incorrect!'; 
  }
  
if ( !isset($textarea) || empty($textarea) ) {
    $ok = false;
    $messages[] = 'Fill in textarea!';
}
echo json_encode(
    array(
        'ok' => $ok,
        'messages' => $messages
    )
);
