
function validateForm() {
  var x = document.getElementById('textarea').value
  var y = document.getElementById('email').value
  var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (x == null || x == "" || y == null || y == "") {
    alert("The field must not be empty!")
    return false
  }
  if (regex.test(y)) {
    return (true)
  }
  alert("You have entered an invalid email address!")
  return (false)
}

