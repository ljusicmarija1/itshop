-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2022 at 03:16 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it-shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `id_manufacturer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `id_manufacturer`) VALUES
(1, 'Huawei', 3),
(2, 'Samsung', 1),
(3, 'iPhone', 2),
(4, 'Lenovo', 5),
(5, 'HP', 6),
(6, 'Acer', 7),
(7, 'Dell', 8),
(8, 'Xiaomi', 4),
(9, 'Hisense', 9);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id_manufacturer` int(11) NOT NULL,
  `manufacturer_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id_manufacturer`, `manufacturer_name`) VALUES
(1, 'Samsung'),
(2, 'Apple'),
(3, 'Huawei'),
(4, 'Xiaomi'),
(5, 'Lenovo'),
(6, 'HP'),
(7, 'Acer'),
(8, 'Dell'),
(9, 'Hisense');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_detail_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `odrder_detail_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`odrder_detail_id`, `first_name`, `last_name`, `phone_number`, `address`, `city`) VALUES
(1, 'Darko', 'Djapovic', '0600180819', 'Uzicke republike 83', 'Uzice');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_2` longtext NOT NULL,
  `image_3` longtext NOT NULL,
  `image_4` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `id_type`, `price`, `name`, `model`, `color`, `id_brand`, `image`, `image_2`, `image_3`, `image_4`) VALUES
(9, 1, 110000, 'iPhone', '13 Pro', 'blue', 3, 'iphone.png', 'https://uploads-ssl.webflow.com/5c6a871c80da2a2eb16e5070/6149d612b38c6e085c986144_iphone%2013%20pro%201.jpg', 'https://myitstore.com.pk/wp-content/uploads/2022/03/apple-iphone-13-pro-max-1-600x457.jpg', 'https://kwcellular.ca/wp-content/uploads/2022/02/iphone-13-pro.jpg'),
(10, 1, 42000, 'Xiaomi', 'Note 11 Pro', 'blue', 8, 'xiaomi-blue.png', 'https://img.gigatron.rs/img/products/large/image625faa88c90d4.png', 'https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/redmi_note_11_pro_5g_1.PNG', 'https://mi-srbija.rs/storage/product/images/56/d3/redmi-note-11-pro-5g-701-701-3378.png'),
(11, 1, 62000, 'Xiaomi', 'Mi 11T', 'white', 8, 'mi-11t.png', 'https://mi-srbija.rs/storage/product/images/25/0b/xiaomi-11t-3314.png', 'https://mi-srbija.rs/storage/product/images/45/fd/xiaomi-11t-3363.png', 'https://mi-srbija.rs/storage/product/images/2e/af/mi-11t-1923.png'),
(12, 3, 56500, 'Lenovo', 'AMD', 'black', 4, 'lenovo.png', 'https://www.laptopplaza.rs/images/detailed/43/Lenovo-ThinkPad-X13-b.png', 'https://www.laptopplaza.rs/images/detailed/44/image5f6c943584767_s5yi-x7.png', 'https://www.lenovo.com/medias/lenovo-laptop-thinkpad-L13-Gen2-AMD-hero.png?context=bWFzdGVyfHJvb3R8MjQ5NDQ1fGltYWdlL3BuZ3xoNzkvaDcxLzE0Mjg2MDg2OTYzMjMwLnBuZ3wyYWViNzEyYmQ4YjYwMDZhNjc0YjQ2MGRiZmU0ZTc3OTZmNDc3YTA0N2JjMDA5NGI5ZjhiMWFiNWQxOTBkYTkz'),
(13, 3, 88000, 'HP', 'AMD', 'silver', 5, 'hp.png', 'https://img.gigatron.rs/img/products/large/image5d7908227cecc.png', 'https://img.gigatron.rs/img/products/large/image5e25a819c3b3b.png', 'https://img.gigatron.rs/img/products/large/image5d5feb75938d9.png'),
(14, 3, 31000, 'Acer', 'Intel', 'silver', 6, 'acer.png', 'https://img.gigatron.rs/img/products/large/ACER-Aspire-5-A515-45-NX.A84EX.004-67.png', 'https://img.gigatron.rs/img/products/large/image594318a2eda66.png', 'https://img.gigatron.rs/img/products/large/ACER-Aspire-5-A515-45-NX.A84EX.004-.jpg'),
(15, 3, 84300, 'Dell', 'AMD', 'black', 7, 'dell.png', '', '', ''),
(16, 2, 46000, 'Samsung', 'LCD', 'black', 2, 'samsung.png', '', '', ''),
(17, 2, 100000, 'Samsung', 'QLED', 'white', 2, 'samsung-qled.png', '', '', ''),
(18, 2, 50000, 'Hisense', 'LED', 'silver', 9, 'hisense.png', '', '', ''),
(19, 1, 30000, 'Huawei', 'P30 Lite', 'black', 1, 'p30-lite-black.png', '', '', ''),
(22, 1, 50000, 'Huawei', 'P30 Lite', 'white', 1, 'p30-lite-white.png', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id_product_type` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id_product_type`, `type`) VALUES
(1, 'mobile'),
(2, 'tv'),
(3, 'laptop');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_user_attributes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `type`, `username`, `password`, `id_user_attributes`) VALUES
(1, 'buyer', 'mare99', 'Marko99ue', 1),
(2, 'buyer', 'ica99', 'Ivana99ue', 2),
(3, 'admin', 'darko99', 'Darko99ue', 3),
(4, 'seller', 'ivanap00', 'Ivana00ic', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_attributes`
--

CREATE TABLE `user_attributes` (
  `id_user_attribute` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_attributes`
--

INSERT INTO `user_attributes` (`id_user_attribute`, `first_name`, `last_name`, `email`, `phone_number`) VALUES
(1, 'Marko', 'Marinovic', 'mare@gmail.com', '0600192384'),
(2, 'Ivana', 'Ivanovic', 'ica@gmail.com', '064213255'),
(3, 'Darko', 'Djapovic', 'djapoviccdarko@gmail.com', '0600180819'),
(4, 'Ivana', 'Perisic', 'ivanap.00@gmail.com', '069664992');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`type`) VALUES
('admin'),
('buyer'),
('seller');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `id_manufacturer` (`id_manufacturer`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id_manufacturer`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `product_type_id` (`product_type_id`,`product_id`,`order_detail_id`),
  ADD KEY `order_detail_id` (`order_detail_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`odrder_detail_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_brand` (`id_brand`),
  ADD KEY `id_brand_2` (`id_brand`),
  ADD KEY `id_image` (`image`),
  ADD KEY `type` (`id_type`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id_product_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `type` (`type`,`id_user_attributes`),
  ADD KEY `id_user_attributes` (`id_user_attributes`);

--
-- Indexes for table `user_attributes`
--
ALTER TABLE `user_attributes`
  ADD PRIMARY KEY (`id_user_attribute`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id_manufacturer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `odrder_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id_product_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_attributes`
--
ALTER TABLE `user_attributes`
  MODIFY `id_user_attribute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`id_manufacturer`) REFERENCES `manufacturers` (`id_manufacturer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`order_detail_id`) REFERENCES `order_details` (`odrder_detail_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`id_product_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_brand`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `product_types` (`id_product_type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`type`) REFERENCES `user_types` (`type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_user_attributes`) REFERENCES `user_attributes` (`id_user_attribute`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
